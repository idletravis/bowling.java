package bowling.frame;

import bowling.roll.Bowl;

public class Frame {

    protected Bowl firstBowl;
    protected Bowl secondBowl;


    public Bowl record(Bowl bowl) {
        if (null == firstBowl) {
            firstBowl = bowl;
        } else if (null == secondBowl) {
            secondBowl = bowl;
        } else {
            throw new IndexOutOfBoundsException("Cannot bowl more than twice in a frame");
        }
        return bowl;
    }

    public Bowl getLastBowl() {
        return secondBowl;
    }
}
