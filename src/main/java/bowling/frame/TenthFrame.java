package bowling.frame;

import bowling.roll.Bowl;
import bowling.roll.Spare;

public class TenthFrame extends Frame {

    private Bowl thirdBowl;

    @Override
    public Bowl record(Bowl bowl) {
        try {
            super.record(bowl);
        } catch (IndexOutOfBoundsException boundsException) {
            recordExtraBowl(bowl, boundsException);
        }
        return bowl;
    }

    private void recordExtraBowl(Bowl bowl, IndexOutOfBoundsException boundsException) {
        switch (secondBowl) {
            case Spare spare -> {
                spare.setNextBowl(bowl);
                thirdBowl = bowl;
            }
            case null, default -> throw boundsException;
        }
    }

    @Override
    public Bowl getLastBowl() {
        return thirdBowl;
    }
}
