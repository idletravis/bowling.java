package bowling;

import bowling.frame.Frame;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

public class BowlingGame {
    public static final String PLAYERS_CANNOT_BE_NULL = "Players cannot be null.";
    public static final String AT_LEAST_ONE_PLAYER = "A bowling game requires at least one player.";
    private List<Player> players;
    private List<Frame> frames;

    public BowlingGame(Player... player) throws IllegalArgumentException {
        List<Player> startingPlayers = new ArrayList<>(Arrays.asList(player));
        checkPlayersNotNullOrEmpty(startingPlayers);
        players = startingPlayers;
        frames = newGameFrames();
    }

    public BowlingGame(List<Player> startingPlayers) {
        this(startingPlayers.toArray(Player[]::new));
    }

    private List<Frame> newGameFrames() {
        return IntStream.range(0, 10).mapToObj(i -> new Frame()).toList();
    }

    public BowlingGame addPlayer(Player newPlayer) {
        checkPlayerNotNull(newPlayer);
        players.add(newPlayer);
        return this;
    }

    private static void checkPlayersNotNullOrEmpty(List<Player> startingPlayers) {
        if (startingPlayers.isEmpty()) throw new IllegalArgumentException(AT_LEAST_ONE_PLAYER);
        if (startingPlayers.contains(null)) throw new IllegalArgumentException(PLAYERS_CANNOT_BE_NULL);
    }

    private static void checkPlayerNotNull(Player newPlayer) {
        if (null == newPlayer) throw new IllegalArgumentException(PLAYERS_CANNOT_BE_NULL);
    }

    public List<Player> getPlayers() {
        return players;
    }

    public List<Frame> getFrames() {
        return frames;
    }

    public void setFrames(List<Frame> frames) {
        this.frames = frames;
    }
}
