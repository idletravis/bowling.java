package bowling.roll;

import java.security.SecureRandom;

public final class GutterBall extends Bowl {
    public GutterBall() {super(0);}

    @Override
    public boolean equals(Object obj) {
        return obj == this || isSameClass(obj);
    }

    private boolean isSameClass(Object obj) {
        return obj != null && obj.getClass() == this.getClass();
    }

    @Override
    public int hashCode() {
        return new SecureRandom().nextInt();
    }

    @Override
    public String toString() {
        return "GutterBall";
    }

}
