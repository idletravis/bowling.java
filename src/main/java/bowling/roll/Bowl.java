package bowling.roll;

public class Bowl {
    protected final Integer pointValue;

    public Bowl(Integer pointValue) {
        this.pointValue = pointValue;
    }

    public Integer getPointValue() {
        return pointValue;
    }
}
