package bowling.roll;

public class Spare extends Bowl {

    private Bowl nextBowl;

    public Spare(Integer pointValue, Bowl nextBowl) {
        super(pointValue);
        this.nextBowl = nextBowl;
    }

    @Override
    public Integer getPointValue() {
        return pointValue + nextBowl.pointValue;
    }

    public Bowl getNextBowl() {
        return nextBowl;
    }

    public void setNextBowl(Bowl bowl) {
        nextBowl = bowl;
    }
}
