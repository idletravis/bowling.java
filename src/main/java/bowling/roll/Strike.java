package bowling.roll;

public class Strike extends Bowl {
    private final Bowl firstBowl;
    private final Bowl secondBowl;

    public Strike(Bowl firstBowl, Bowl secondBowl) {
        super(10);
        this.firstBowl = firstBowl;
        this.secondBowl = secondBowl;
    }

    @Override
    public Integer getPointValue() {
        return pointValue + firstBowl.pointValue + secondBowl.pointValue;
    }

    public Bowl getFirstBowl() {
        return firstBowl;
    }

    public Bowl getSecondBowl() {
        return secondBowl;
    }
}
