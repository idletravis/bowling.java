package bowling;

import bowling.frame.Frame;
import bowling.roll.Bowl;
import bowling.roll.GutterBall;

public class Player {
    private final String name;

    public Player(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public Bowl bowl(Bowl bowl) {
        return bowl;
    }

    public Bowl bowl(Frame frame) {
        Bowl bowl = new GutterBall();
        frame.record(bowl);
        return bowl;
    }

    public Bowl bowl(Bowl bowl, Frame frame) {
        return frame.record(bowl);
    }
}
