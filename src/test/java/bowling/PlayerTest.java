package bowling;

import bowling.roll.Bowl;
import bowling.roll.GutterBall;
import bowling.roll.Spare;
import bowling.roll.Strike;
import net.jqwik.api.Property;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

class PlayerTest {

    public static final String ONE = "one";

    @ParameterizedTest(name = "Player can {2}")
    @MethodSource("differentBowls")
    void canBowl(Class<? extends Bowl> clazz, Bowl bowl, String simpleName) {
        Player player = new Player("name");
        Bowl justBowled = player.bowl(bowl);

        assertThat(justBowled).isExactlyInstanceOf(clazz);
        assertThat(justBowled.getPointValue()).isEqualTo(bowl.getPointValue());
    }

    private static Stream<Arguments> differentBowls() {
        Bowl five = new Bowl(5);
        GutterBall gutterBall = new GutterBall();
        Spare spareTwoPoints = new Spare(2, new Bowl(3));
        Strike strikeThenGutterSpare = new Strike(new Bowl(0), new Spare(10, new Bowl(2)));
        return Stream.of(
                Arguments.of(Bowl.class, five, Bowl.class.getSimpleName()),
                Arguments.of(GutterBall.class, gutterBall, GutterBall.class.getSimpleName()),
                Arguments.of(Spare.class, spareTwoPoints, Spare.class.getSimpleName()),
                Arguments.of(Strike.class, strikeThenGutterSpare, Strike.class.getSimpleName())
        );
    }
}