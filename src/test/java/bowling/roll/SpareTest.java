package bowling.roll;

import net.jqwik.api.*;

import java.util.Optional;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

class SpareTest {

    @Property
    void nextBowlPinsAreAddedToPointValue(@ForAll("zeroToTen") Integer basePointValue, @ForAll("regularBowlSpares") Bowl nextBowl) {
        Spare spare = new Spare(basePointValue, nextBowl);

        assertThat(spare.getPointValue()).isEqualTo(basePointValue + nextBowl.pointValue);
    }

    @Provide
    Arbitrary<Integer> zeroToTen() {
        return Arbitraries.integers().between(0, 10).shrinkTowards(0);
    }

    @Provide
    Arbitrary<Bowl> regularBowlSpares() {
        Arbitrary<Bowl> regularBowlSpares = zeroToTen().map(pins -> pins == 10
                ? new Strike(new GutterBall(), new GutterBall())
                : new Bowl(pins));
        Optional<Stream<Bowl>> allPossible = regularBowlSpares.allValues();
        Stream<Bowl> bowlStream = allPossible.orElseGet(regularBowlSpares::sampleStream);
        return Arbitraries.of(bowlStream.toList());
    }

}