package bowling.roll;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class GutterBallTest {

    @Test
    void hasZeroPointValue() {
        GutterBall gutterBall = new GutterBall();

        assertThat(gutterBall.getPointValue()).isZero();
    }

    @Test
    void allGutterBallsAreEqual() {
        GutterBall first = new GutterBall();
        GutterBall second = new GutterBall();

        assertThat(first).isEqualTo(second);
    }
}