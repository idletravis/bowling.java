package bowling.roll;

import net.jqwik.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Optional;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

class StrikeTest {

    @Property(generation = GenerationMode.EXHAUSTIVE)
    void nextTwoBowlsPinsAreAddedToPointValue(@ForAll("possibleStrikes") Strike strike) {
        int tenPlusFirstPlusSecond = 10 + strike.getFirstBowl().pointValue + strike.getSecondBowl().pointValue;
        assertThat(strike.getPointValue()).isEqualTo(tenPlusFirstPlusSecond);
    }

    @Provide
    Arbitrary<Integer> zeroToTen() {
        return Arbitraries.integers().between(0, 10).shrinkTowards(0);
    }

    @Provide
    Arbitrary<Strike> possibleStrikes() {
        Arbitrary<Bowl> regularBowls = zeroToTen().map(Bowl::new);
        Arbitrary<Bowl> regularSpares = zeroToTen()
                .flatMap(pins -> zeroToTen()
                        .map(nextBowl -> new Spare(pins, new Bowl(nextBowl))));
        Arbitrary<Strike> regularBowlAndSpareStrikes = Combinators.combine(regularBowls, regularSpares)
                                                          .as(Strike::new);
        Optional<Stream<Strike>> allPossible = regularBowlAndSpareStrikes.allValues();
        Stream<Strike> bowlStream = allPossible.orElseGet(regularBowlAndSpareStrikes::sampleStream);
        return Arbitraries.of(bowlStream.toList());
    }

}