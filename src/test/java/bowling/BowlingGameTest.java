package bowling;

import bowling.BowlingGame;
import bowling.Player;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.*;

class BowlingGameTest {

    public static final String CANNOT_BE_NULL = "cannot be null";
    public static final String ONE = "one";
    public static final String TWO = "two";
    public static final String THREE = "three";

    @Test
    void requiresAtLeastOnePlayer() {
        assertThatThrownBy(() -> new BowlingGame((Player) null))
                .isExactlyInstanceOf(IllegalArgumentException.class)
                .hasMessageContaining(CANNOT_BE_NULL);

        assertThatNoException().isThrownBy(() -> new BowlingGame(new Player(ONE)));
    }

    @Test
    void canHaveMoreThanOnePlayer() {
        // given
        Player player1 = new Player(ONE);
        Player player2 = new Player(TWO);
        Player player3 = new Player(THREE);
        List<Player> players = List.of(player1, player2, player3);
        // then
        assertThatNoException().isThrownBy(() -> {
            // when
            new BowlingGame(player1);
            new BowlingGame(player1, player2);
            new BowlingGame(players);
        });
    }

    @Test
    void canAddPlayerLater() {
        // given
        Player player1 = new Player(ONE);
        Player player2 = new Player(TWO);
        BowlingGame game = new BowlingGame(player1);
        // when
        game.addPlayer(player2);
        // then
        assertThat(game.getPlayers())
                .contains(player2);
    }

    @Test
    void playerAdditionsCannotBeNull() {
        // given
        Player player1 = new Player(ONE);
        Player player2 = new Player(TWO);
        BowlingGame game = new BowlingGame(player1);

        // when
        assertThatThrownBy(() -> game.addPlayer(null))
                // then
                .isExactlyInstanceOf(IllegalArgumentException.class)
                .hasMessageContaining(CANNOT_BE_NULL);

    }

    @Test
    void hasExactlyTenFrames() {
        BowlingGame game = new BowlingGame(new Player(ONE));

        assertThat(game.getFrames()).hasSize(10);
    }
}