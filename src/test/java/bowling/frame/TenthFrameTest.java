package bowling.frame;

import bowling.Player;
import bowling.frame.Frame;
import bowling.frame.TenthFrame;
import bowling.roll.Bowl;
import bowling.roll.Spare;
import net.jqwik.api.*;

import java.util.Optional;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

class TenthFrameTest {

    public static final String ONE = "one";

    @Property
    void tenthFrameHasThreeBowlsOnSpare(@ForAll("simpleBowls") Bowl firstBowl, @ForAll("simpleBowls") Bowl lastBowl) {
        // given
        Frame tenthFrame = new TenthFrame();
        Player player = new Player(ONE);
        Spare secondBowl = new Spare(10 - firstBowl.getPointValue(), null);
        // when
        player.bowl(firstBowl, tenthFrame);
        player.bowl(secondBowl, tenthFrame);
        player.bowl(lastBowl, tenthFrame);
        // then
        assertThat(tenthFrame.getLastBowl()).isSameAs(lastBowl);
    }

    @Provide
    Arbitrary<Integer> zeroToNine() {
        return Arbitraries.integers().between(0, 9).shrinkTowards(0);
    }

    @Provide
    Arbitrary<Bowl> simpleBowls() {
        Arbitrary<Bowl> regularBowlSpares = zeroToNine().map(Bowl::new);
        Optional<Stream<Bowl>> allPossible = regularBowlSpares.allValues();
        Stream<Bowl> bowlStream = allPossible.orElseGet(regularBowlSpares::sampleStream);
        return Arbitraries.of(bowlStream.toList());
    }
}