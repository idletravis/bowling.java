package bowling.frame;

import bowling.Player;
import bowling.frame.Frame;
import net.jqwik.api.ForAll;
import net.jqwik.api.Property;
import net.jqwik.api.constraints.IntRange;

import java.util.stream.IntStream;

import static org.assertj.core.api.Assertions.assertThatThrownBy;

class FrameTest {

    public static final String ONE = "one";

    @Property
    void maximumTwoBowlsPerFrame(@ForAll @IntRange(min = 3) Integer end) {
        Frame frame = new Frame();
        Player player = new Player(ONE);
        assertThatThrownBy(() -> IntStream.range(0, end)
                                          .forEach(i -> player.bowl(frame)))
                .isExactlyInstanceOf(IndexOutOfBoundsException.class);
    }

}